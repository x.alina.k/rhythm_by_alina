# rhythm

> a todo-app that helps you to overcome short-term thinking and follow your longterm goals.

Check it out [here](https://rhythm.alinakarl.com/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
