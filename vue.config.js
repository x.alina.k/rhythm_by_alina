// vue.config.js
module.exports = {
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Rhythm'; // eslint-disable-line no-param-reassign
        return args;
      });
  },
  outputDir: 'dist',
  publicPath: '/',
  pwa: {
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'default',
    manifestOptions: {
      background_color: '#C9D9D6',
      description: 'Focus on what is really important, on building new habits and finding and appreciating your own rhythm.',
      orientation: 'portrait',
      start_url: '/',
      themeColor: '#D4CE21',
    },
    msTileColor: '#C9D9D6',
    name: 'Rhythm',
    workboxOptions: {
      skipWaiting: true,
      exclude: ['_redirects'], // ignor _redirects, to prevent errors in Service Worker
    },
  },
};
