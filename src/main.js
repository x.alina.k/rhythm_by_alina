import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';

// with import 'typeface-inter';  and typeface could be imported, that is installen with nmp
import '@/assets/styles/base.styl';
import '@/assets/styles/breakpoints.styl';

Vue.config.productionTip = false; // don't show warning when in development mode

new Vue({
  router,
  store,
  render: (h) => h(App), // show app component
}).$mount('#app'); // always when the Element with ID 'app' is mencioned in HTML
