// store of variables that is acessable from all components
// good alternative to global variables
// it's a store of variables in the so-called state, that can be changed by mutations and actions
// Changes can only be done by mutations, but they have to be synchronous Asynchronous changes can be written in actions

import Vue from 'vue';
import Vuex from 'vuex';
import db from '@/db.js';

Vue.use(Vuex); // vue shall use vuex

// initialize new vuex store
const store = new Vuex.Store({ // global variables have to be defined here, even if they don't have a value
  state: {
    onboardingComplete: false,
    installPrompt: null,
    loadedTodos: [],
    projects: [],
    categorie: [],
    motives: [],
    toasts: [],
    timeExpected: [],
    timeNeeded: [],
    settings: {
      name: '',
    },
  },
  mutations: {
    addToast(state, toast) {
      // if there's no timeout defined, add one
      if (!toast.timeout) toast.timeout = 5000; // eslint-disable-line no-param-reassign
      // if there's no ID on the toast add one
      if (!toast.id) toast.id = Math.random().toString(36).substring(2, 9); // eslint-disable-line no-param-reassign
      // add toast-object to toast-array in state
      state.toasts.unshift(toast);
    },
    removeToast(state, id) {
      // find index of toast through it's ID
      const index = state.toasts.findIndex((existingToast) => existingToast.id === id);
      // delet toast at this index from state
      state.toasts.splice(index, 1);
    },
    insertTodo(state, todo) {
      state.loadedTodos.unshift(todo); // shows the last added first
    },
    setTodos(state, todos) {
      state.loadedTodos = todos;
    },
    deleteTodo(state, id) {
      const todoIndex = state.loadedTodos.findIndex((existingTodo) => existingTodo.id === id);
      state.loadedTodos.splice(todoIndex, 1);
    },
    deleteProject(state, id) {
      // search todo in loaded todos
      const projectIndex = state.projects.findIndex((project) => project.id === id);
      // and delete it in database
      const cleanTodos = state.loadedTodos.filter((todo) => {
        if (id !== todo.project) return true;
        return false;
      });
      state.loadedTodos = cleanTodos;
      state.projects.splice(projectIndex, 1);
    },
    checkTodo(state, id) {
      const todo = state.loadedTodos.find((existingTodo) => existingTodo.id === id);
      todo.done = !todo.done;
    },
    updateTodo(state, data) {
      const index = state.loadedTodos.findIndex((existingTodo) => existingTodo.id === data.todoId);
      // vue doesn't notice changes in objects (and arrays) vm.$set is used to communicate the changes
      Vue.set(state.loadedTodos, index, { ...state.loadedTodos[index], ...data.changes });
    },
    updateProject(state, data) {
      const index = state.projects.findIndex((existingProject) => existingProject.id === data.projectId);
      Vue.set(state.projects, index, { ...state.projects[index], ...data.changes });
    },
    insertProject(state, project) {
      state.projects.unshift(project);
    },
    setProjects(state, projects) {
      state.projects = projects;
    },
    insertMotive(state, motive) {
      state.motives.unshift(motive);
    },
    setMotives(state, motives) {
      state.motives = motives;
    },
    setCategory(state, category) {
      state.category = category;
    },
    addCategory(state, category) {
      state.categorie.push(category);
    },
    setSetting(state, payLoad) {
      state.settings[payLoad.key] = payLoad.value;
    },
    setOnboardingComplete(state, value) {
      state.onboardingComplete = value;
    },
    setInstallPrompt(state, prompt) {
      state.installPrompt = prompt;
    },
    setName(state, value) {
      state.settings.name = value;
    },
  },
  getters: {
    doneTodos(state) {
      return state.loadedTodos.filter((todo) => {
        if (todo.done) return true;
        return false;
      });
    },
    undoneTodos(state) {
      return state.loadedTodos.filter((todo) => !todo.done);
    },
  },
  actions: {
    async addTodo(context, todo) {
      if (!todo.createdAt) todo.createdAt = Date.now(); // eslint-disable-line no-param-reassign
      if (!todo.dueDate) todo.dueDate = Date.now() + 24 * 3600 * 1000; // eslint-disable-line no-param-reassign
      if (!todo.done) todo.done = false; // eslint-disable-line no-param-reassign
      const id = await db.todos.add(todo);
      const todoWithId = { ...todo, id };
      context.commit('insertTodo', todoWithId); // calls mutation that writes value in state
    },
    async loadTodos(context) {
      const todos = await db.todos.orderBy('text').toArray(); // .reverse() wenn nicht richtig
      context.commit('setTodos', todos);
    },
    async loadTodosForProject(context, projectId) {
      const todos = await db.todos.where('project').equals(projectId).sortBy('dueDate');
      context.commit('setTodos', todos);
    },
    async addProject(context, project) {
      const id = await db.projects.add(project);
      const projectWithId = { ...project, id };
      context.commit('insertProject', projectWithId);
      return id;
    },
    async loadProjects(context) {
      const projects = await db.projects.orderBy('name').toArray();
      context.commit('setProjects', projects);
    },
    async addMotive(context, motive) {
      const id = await db.motives.add(motive);
      const motiveWithId = { ...motive, id };
      context.commit('insertMotive', motiveWithId);
      return id;
    },
    async loadMotives(context) {
      const motives = await db.motives.orderBy('text').toArray();
      context.commit('setMotives', motives);
    },
    async deleteTodo(context, todoId) {
      // delet todo in database
      await db.todos.delete(todoId);
      context.commit('deleteTodo', todoId);
    },
    async checkTodo(context, todoId) {
      await db.todos.update(todoId, { done: true });
      context.commit('checkTodo', todoId);
    },
    async updateTodo(context, data) {
      await db.todos.update(data.todoId, data.changes);
      context.commit('updateTodo', data);
    },
    async updateProject(context, data) {
      await db.projects.update(data.projectId, data.changes);
      context.commit('updateProject', data);
    },
    async deleteProject(context, projectId) {
      await db.projects.delete(projectId);
      await db.todos.where('project').equals(projectId).delete();
      context.commit('deleteProject', projectId);
    },
    async loadTodosForCategory(context, option) {
      const loadedTodos = await db.todos.where('category').equalsIgnoreCase(option).toArray();
      context.commit('setTodos', loadedTodos);
    },
    async setOnboardingComplete(context, value) {
      await db.settings.put({ name: 'onboardingComplete', value }); // save settings with the name onboardingComplete and the value 'value'
      context.commit('setOnboardingComplete', value);
    },
    // save settings with name 'username' and the value 'value'
    // in this case it's value:value (this is just the short version)
    // table put adds a element when the ID (in this case 'name') doesn't exist
    // otherwise the object is replaced by the ID
    async addName(context, value) {
      await db.settings.put({ name: 'username', value });
      context.commit('setName', value); // call mutation that adds the value to the state
    },
  },
});

export default store;
