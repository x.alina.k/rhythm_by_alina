// initialize the database and specify the schema of Data to be stored in this database.
// export a reference to this database to use it at other places

import Dexie from 'dexie';

const db = new Dexie('TodoDatabase'); // create database, name isn't important, and save it in the variable 'db'
/*
// data is saved in tables, f.e '++id' is a column, data or properties to be searched for or sorted by has to be declared as a column
// columns are named index and the first column is the primary index or primary key

todos:

{
id: ID
text: String,
done: Boolean,
createdAt: Number / Date
project: ID // ein todo kann immer nur ein einem Projekt sein, in Klammern wäre mehrere
}
projekte {
id: ID
namen: String
}

*/

// wenn man nach etwas sortieren will braucht man einen index

db.version(1).stores({
  todos: '++id,text,project,createdAt,category,dueDate,done,motive,hoursTotal,hoursWorked', // was für Tabellen man braucht, ++ zeigt an, dasss ID immer Einzigartig
  projects: '++id, name', // the primary key is an automatially generated key by dexie (++)
  motives: '++id, text',
  settings: 'name', // the primary key is an unique string
});

export default db; // export the database to use it in other scripts
