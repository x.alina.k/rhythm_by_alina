import Vue from 'vue';
import VueRouter from 'vue-router';

import db from '@/db';

import Home from '../views/Home.vue';
import Timediagrams from '../views/Timediagrams.vue';
import Settings from '../views/Settings.vue';
import Projects from '../views/Projects.vue';
import Onboarding from '../views/Onboarding.vue';
import Category from '../views/Category.vue';


Vue.use(VueRouter); // vue shall use router

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    async beforeEnter(to, from, next) {
      // the setting is loaded
      const onboardingComplete = await db.settings.get('onboardingComplete');
      // if the setting doesn't exist or if the value isn't true the route changes
      if (!onboardingComplete || !onboardingComplete.value) next({ name: 'onboarding' });
      // otherwise change to home
      else next();
    },
  },
  {
    path: '/timediagrams',
    name: 'timediagrams',
    component: Timediagrams,
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
  },
  {
    path: '/projects',
    name: 'projects',
    component: Projects,
  },
  {
    path: '/category/:option',
    name: 'category',
    component: Category,
  },
  {
    path: '/onboarding',
    name: 'onboarding',
    component: Onboarding,
    async beforeEnter(to, from, next) {
      // if onboarding was already shown change to home
      const onboardingComplete = await db.settings.get('onboardingComplete');
      if (onboardingComplete && onboardingComplete.value) next({ name: 'home' });
      else next();
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
